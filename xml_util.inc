<?php


/* written by Kris: http://www.devdump.com/phpxml.php 
   UTF-8 is hardcoded
*/

/** 
 * Print content of $obj array in readable form, alternative to print_r()
 */
function print_a($obj) {
  global $__level_deep;
  if (!isset($__level_deep)) $__level_deep = array();

  if (is_object($obj))
    print '[obj]';
  elseif (is_array($obj)) {
    foreach(array_keys($obj) as $keys) {
      array_push($__level_deep, "[".$keys."]");
      print_a($obj[$keys]);
      array_pop($__level_deep);
    }
  }
  else print implode(" ",$__level_deep)." = $obj \n";
}

define ('XML_ATTRIBUTES', '*ATTRIBUTES*');
define ('XML_VALUE', '*VALUE*');
define ('XML_FLAGS', '*FLAGS*');

define ('XML_FLAGS_BR', 1);           // add leading a trailing CR to content of element
define ('XML_FLAGS_REMOVE_EMPTY', 2); // do not output element/attributes if content is empty string 


function _xml_get_children($vals, &$i) 
{ 
  $children = array();     // Contains node data
  
  /* Node has CDATA before it's children */
  if (isset($vals[$i]['value'])) 
    $children[XML_VALUE] = _xml_xml2str($vals[$i]['value']); 
  
  /* Loop through children */
  while (++$i < count($vals))
  { 
    switch ($vals[$i]['type']) 
    { 
      /* Node has CDATA after one of it's children 
        (Add to cdata found before if this is the case) */
      case 'cdata': 
        if (isset($children['VALUE']))
          $children[XML_VALUE] .= _xml_xml2str($vals[$i]['value']); 
        else
          $children[XML_VALUE] = _xml_xml2str($vals[$i]['value']); 
        break;
      /* At end of current branch */ 
      case 'complete': 
        if (isset($vals[$i]['attributes'])) {
          $children[$vals[$i]['tag']][][XML_ATTRIBUTES] = $vals[$i]['attributes'];
          $index = count($children[$vals[$i]['tag']])-1;

          if (isset($vals[$i]['value'])) 
            $children[$vals[$i]['tag']][$index][XML_VALUE] = _xml_xml2str($vals[$i]['value']); 
          else
            $children[$vals[$i]['tag']][$index][XML_VALUE] = ''; 
        } else {
          if (isset($vals[$i]['value'])) 
            $children[$vals[$i]['tag']][][XML_VALUE] = _xml_xml2str($vals[$i]['value']); 
          else
            $children[$vals[$i]['tag']][][XML_VALUE] = ''; 
		}
        break; 
      /* Node has more children */
      case 'open': 
        if (isset($vals[$i]['attributes'])) {
          $children[$vals[$i]['tag']][][XML_ATTRIBUTES] = $vals[$i]['attributes'];
          $index = count($children[$vals[$i]['tag']])-1;
          $children[$vals[$i]['tag']][$index] = array_merge($children[$vals[$i]['tag']][$index], _xml_get_children($vals, $i));
        } else {
          $children[$vals[$i]['tag']][] = _xml_get_children($vals, $i);
        }
        break; 
      /* End of node, return collected data */
      case 'close': 
        return $children; 
    } 
  } 
} 

/** 
 * Converts xml data into tree of arrays 
 */
function _xml_xml2tree($xml) 
{
  $parser = xml_parser_create('UTF-8');
  xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 0);    // do not skip CRLF and spaces
  xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);  // do not uppercase
  xml_parse_into_struct($parser, $xml, $vals, $index); 
  xml_parser_free($parser); 

  $tree = array(); 
  $i = 0; 
  
  if (isset($vals[$i]['attributes'])) {
	$tree[$vals[$i]['tag']][][XML_ATTRIBUTES] = $vals[$i]['attributes']; 
	$index = count($tree[$vals[$i]['tag']])-1;
	$tree[$vals[$i]['tag']][$index] =  array_merge($tree[$vals[$i]['tag']][$index], _xml_get_children($vals, $i));
  }
  else
    $tree[$vals[$i]['tag']][] = _xml_get_children($vals, $i); 
  
  return $tree; 
} 

/**
 * Strip leading and trailling CR/LF, potentially added XML_FLAGS_BR
 */
function _xml_xml2str($s) {
  $s = preg_replace(array('/^( *\\n *)/', '/( *\\n *$)/'), array('', ''), $s);
  return $s;
}
 
/** 
 * Converts non allowed chars to entities
 */
function _xml_str2xml($s, $quo = true) {
  $search = array('/&/', '/>/', '/</');
  $replace = array('&amp;', '&gt;', '&lt;');
  if ($quo) {
    $search[] = '/"/';
    $replace[] = '/&quo;/';
  }
  return preg_replace($search, $replace, $s);
}

/** 
 * Returns xml tag, optionally with attributes
 */

function _xml_write_element(&$output, $tag, $attributes, $flags, $type, $level) {
  if ($level) {
    $output .= str_repeat('  ', $level);
  }
  $output .= '<';
  if ($type == 2) {
    $output .= '/';
  }
  $output.= $tag;
  if ($attributes) {
    foreach ($attributes as $attr=>$val) {
      if ( !(Trim($val)=='' && ($flags[$attr] & XML_FLAGS_REMOVE_EMPTY)) ) {
        $output .= ' '.$attr.'="'._xml_str2xml($val) .'"';
      }
    }
  }
  if ($type == 1) {
    $output .= ' /';
  }
  $output .= '>';
  if ($type != 0) {
    $output .= "\n";
  }
}

/** 
 * Converts tree of arrays into xml data 
 */

function _xml_tree2xml(&$output, $tree, $level=0) {
  foreach ($tree as $tag=>$items) {
    if ($tag!=XML_ATTRIBUTES && $tag!=XML_FLAGS && Is_Array($items)) {
      foreach ($items as $item) {
        $n = Count($item);
        foreach (array(XML_ATTRIBUTES, XML_VALUE, XML_FLAGS) as $at) {
          if (IsSet($item[$at])) {
            $n--;
          }
        }
        $exp_fl = !(Trim($item[XML_VALUE])=='' && ($item[XML_FLAGS][XML_VALUE] & XML_FLAGS_REMOVE_EMPTY));
        _xml_write_element($output, $tag, $item[XML_ATTRIBUTES], $item[XML_FLAGS][XML_ATTRIBUTES], ($n>0 || $exp_fl)?0:1, $level);
        if ($n>0 || $exp_fl) {
          if ($n>0 || ($item[XML_FLAGS][XML_VALUE] & XML_FLAGS_BR)) {
            $output .= "\n";
          }
          if ( Trim($item[XML_VALUE]) != '' ) {
            $output .= _xml_str2xml($item[XML_VALUE], false);
            if (($n>0 || $item[XML_FLAGS][XML_VALUE] & XML_FLAGS_BR)) {
              $output .= "\n";
            }
          }
          _xml_Tree2XML($output, $item, $level+1);
          _xml_write_element($output, $tag, 0, $item[XML_FLAGS][XML_ATTRIBUTES], 2, ($n==0 && $exp_fl && ($item[XML_FLAGS][XML_VALUE] & XML_FLAGS_BR)==0)?0:$level);
        }      
      }
    }
  }
}

/** 
 * Converts xml data into tree of arrays. Data must be UTF-8 encoded
 */

function xml_decode_xml($xml) {
  return _xml_xml2tree($xml);
}

/** 
 * Converts array into XML data
 */

function xml_encode_xml($tree) {
  $output = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
  _xml_tree2xml($output, $tree);
  return $output;
}

?>
