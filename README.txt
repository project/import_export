Import export module for Drupal
===============================

Description:
------------
        
The module should simplify mass manual task. Now it's possible to
maintain nodes separately from web browser and import them all in the
same time automatically. 

You can also: export selected nodes, process in a word processor
(grammar check, ...) and import in another task.


Import/export from a CSV file or a XML formated file is supported. The
CSV is suitable for simple nodes (weblinks, images, etc.), the XML for
large ones (stories, books, etc.).

Currently is built-in support for common nodes, stories, books, pages, images, flexinodes
directly into module because I have not core (almost-core) modules under control. 
Custom modules should implement import/export support via nodeapi hooks (see
weblink, keyword_links).

Note that files must be UTF-8 encoded.

Changes
-------

8/2005 - flexinode support (should be reimplemented as hook directly to flexinode (and image))

Authors
-------
Tomas Mandys (tomas.mandys at 2p.cz)

